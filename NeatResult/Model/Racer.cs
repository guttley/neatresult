﻿namespace NeatResult.Model
{

    /// <summary>
    /// Encapsulates the info about a racer to be renderd
    /// </summary>
    public class Racer
    {
        public int Position { get; set; }
        public string Name { get; set; }
        public int Points { get; set; }
    }
}