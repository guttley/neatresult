﻿using NeatResult.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeatResult
{
    /// <summary>
    /// This class is responsible for generating the graphic output
    /// </summary>
    internal class ResultGenerator : IDisposable
    {
        #region Private vars
        /// <summary>
        /// Base image for render all the results too
        /// </summary>
        private Bitmap _BaseImage;

        /// <summary>
        /// The current Y postion
        /// </summary>
        private int _CurrentY;
        #endregion

        #region Public properties
        /// <summary>
        /// Path to the backing graphic
        /// </summary>
        public string GraphicPath { get; set; }

        /// <summary>
        /// Name of the championship. This is the main banner
        /// </summary>
        public string ChampionshipName { get; set; }

        /// <summary>
        /// Championship round. This is the sub banner
        /// </summary>
        public string ChampionshipRound { get; set; }

        /// <summary>
        /// Results to render
        /// </summary>
        public List<Racer> Results { get; set; }

        /// <summary>
        /// Width of the rendered result grahic
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Heigth of the rendered result grahic
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// The border on the X (right/left) axis
        /// </summary>
        public int BorderX { get; set; }

        /// <summary>
        /// The border on the Y (top/bottom) axis
        /// </summary>
        public int BorderY { get; set; }

        /// <summary>
        /// The margin between rows
        /// </summary>
        public int RowMargin { get; set; }

        /// <summary>
        /// Background colour for text
        /// </summary>
        public Brush TextBackground { get; set; }

        /// <summary>
        /// Background colour for position element of a racer row
        /// </summary>
        public Brush PositionBackground { get; set; }

        /// <summary>
        /// Colour of text
        /// </summary>
        public Brush FontColor { get; set; }

        /// <summary>
        /// Font for the rendered text
        /// </summary>
        public FontFamily Font { get; set; }

        /// <summary>
        /// List of resultant images that have the results rendered to them
        /// </summary>
        public List<Bitmap> ResultImages { get; private set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ResultGenerator()
        {
            //
            //  Setup some reasonable defaults
            //
            Width = 1920;
            Height = 1080;
            BorderX = (int)(Width * 0.1);
            BorderY = (int)(Height * 0.1);
            RowMargin = 10;            
            TextBackground = new SolidBrush(Color.FromArgb(192, Color.Black));
            PositionBackground = new SolidBrush(Color.FromArgb(230, Color.Gray));
            Font = FontFamily.GenericSansSerif;
            FontColor = Brushes.White;
        }
        #endregion

        #region Private Methods       
        /// <summary>
        /// Generates the base image to manipulate
        /// </summary>
        /// <param name="graphicPath">Path the backing graphic</param>
        /// <param name="width">Width of the desired output image</param>
        /// <param name="height">Height of the desired output image</param>
        private Bitmap GetBaseImage()
        {
            return (Bitmap)Bitmap.FromFile(GraphicPath);

            /*
            var bmp = new Bitmap(Width, Height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, Width, Height);
                graph.FillRectangle(Brushes.AntiqueWhite, ImageSize);
            }
            return bmp;
            */
        }

        /// <summary>
        /// Super imposes bitmapB on to bitmapA at the specified coordinates
        /// </summary>
        /// <param name="bitmapA">The larger bitmap to be used as the background</param>
        /// <param name="bitmapB">The smaller bitmap to be super imposed</param>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        private void SuperImpose(Bitmap bitmapA, Bitmap bitmapB, int x, int y)
        {
            using (Graphics g = Graphics.FromImage(bitmapA))
            {
                g.CompositingMode = CompositingMode.SourceOver;
                g.DrawImage(bitmapB, new Point(x, y));
            }
        }

        /// <summary>
        /// Retuns the width of a single element. An element being a single item rendered to the base image
        /// </summary>
        private int GetTextBackgroundWidth()
        {
            return Width - (BorderX * 2);
        }

        /// <summary>
        /// Returns the background for a single text element
        /// </summary>
        /// <param name="width">Width of the element</param>
        /// <param name="height">Height of the elemet</param>
        /// <returns></returns>
        private Bitmap GetTextBackground(int width, int height)
        {
            var bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(TextBackground, ImageSize);
            }
            return bmp;
        }

        /// <summary>
        /// Returns the background for a single racer element
        /// </summary>
        /// <param name="width">Width of the element</param>
        /// <param name="height">Height of the elemet</param>
        /// <returns></returns>
        private Bitmap GetRacerBackground(int width, int height)
        {
            var bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(TextBackground, ImageSize);


                var posPoly = new Point[]
                {
                    new Point(0,0),
                    new Point(75,0),
                    new Point(50,height),
                    new Point(0,height),
                };

                graph.FillPolygon(PositionBackground, posPoly);
            }
            return bmp;
        }        

        /// <summary>
        /// Renders text on to the provided bitmap
        /// </summary>
        /// <param name="bmp">Bitmap to render the text on to</param>
        /// <param name="text">Text to render</param>
        /// <param name="fontSize">Font size</param>
        /// <param name="x">Font position</param>
        private void RenderText(Bitmap bmp, string text, int fontSize, int x)
        {
            var stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Center
            };

            using (var graphic = Graphics.FromImage(bmp))
            {
                graphic.SmoothingMode = SmoothingMode.AntiAlias;
                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.DrawString(text, new Font(Font, fontSize), FontColor, new PointF(x, bmp.Height / 2), stringFormat);
                graphic.Flush();
            }
        }

        /// <summary>
        /// Renders the ChampionshipBanner
        /// </summary>
        private void RenderChampionshipBanner()
        {
            //
            //  Get the banner background
            //
            var banner = GetTextBackground(GetTextBackgroundWidth(), 60);

            //
            //  Render the text to the banner
            //
            RenderText(banner, ChampionshipName, 26, 10);

            //
            //  Render the banner to the base image
            //
            SuperImpose(_BaseImage, banner, BorderX, _CurrentY);

            //
            //  Increment the current Y position
            //
            _CurrentY += banner.Height + RowMargin;
        }

        /// <summary>
        /// Renders the Round banner text
        /// </summary>
        private void RenderRoundBanner()
        {
            //
            //  Get the banner background
            //
            var banner = GetTextBackground(GetTextBackgroundWidth(), 40);

            //
            //  Render the text to the banner
            //
            RenderText(banner, ChampionshipRound, 18, 10);

            //
            //  Render the banner to the base image
            //
            SuperImpose(_BaseImage, banner, BorderX, _CurrentY);

            //
            //  Increment the current Y position
            //
            _CurrentY += banner.Height + RowMargin;
        }

        /// <summary>
        /// Renders the racer to the base image
        /// </summary>
        /// <param name="racer">Racer to render</param>
        private void RenderRacer(Racer racer)
        {
            //
            //  Get the banner background
            //
            var banner = GetRacerBackground(GetTextBackgroundWidth(), 40);

            //
            //  Render the position, name and points to the banner
            //
            RenderText(banner, racer.Position.ToString(), 18, 10);
            RenderText(banner, racer.Name, 18, 100);
            RenderText(banner, racer.Points.ToString(), 18, 1400);


            //
            //  Render the banner to the base image
            //
            SuperImpose(_BaseImage, banner, BorderX, _CurrentY);

            //
            //  Increment the current Y position
            //
            _CurrentY += banner.Height + RowMargin;
        }

        /// <summary>
        /// Builds the base image
        /// </summary>
        private void BuildBaseImage()
        {
            _CurrentY = BorderY;

            //
            //  Get the base image to render upon
            //
            _BaseImage = GetBaseImage();

            //
            //  Render the Championship header
            //
            RenderChampionshipBanner();

            //
            //  Render the round sub header
            //
            RenderRoundBanner();
        }
        #endregion

        /// <summary>
        /// Generates the results output image
        /// </summary>
        internal void Generate()
        {
            ResultImages = new List<Bitmap>();
            BuildBaseImage();

            //
            //  Render each racer to the base image
            //
            foreach(var racer in Results.OrderBy(r => r.Position))
            {
                RenderRacer(racer);

                if (_CurrentY > Height - BorderY)
                {
                    //
                    //  Save the existing image
                    //
                    ResultImages.Add(_BaseImage);

                    //
                    //  Reset for the next set of racers
                    //
                    BuildBaseImage();
                }
            }

            //
            //  Save the existing image
            //
            ResultImages.Add(_BaseImage);
        }

        /// <summary>
        /// Disposes of the images
        /// </summary>
        public void Dispose()
        {
            if (ResultImages == null)
                return;

            foreach (var img in ResultImages)
            {
                img.Dispose();
            }

            ResultImages = null;
        }
    }
}
