﻿using NeatResult.Model;
using System;
using System.Linq;
using System.Collections.Generic;
using OfficeOpenXml;
using System.IO;
using System.Reflection;

namespace NeatResult.ViewModel
{
    /// <summary>
    /// View model for the main view
    /// </summary>
    internal class ViewModelMain : ViewModelBase
    {
        #region Property backing vars
        private bool _Loading;
        private string _SpreadSheetPath;
        private string _GraphicPath;
        private string _ChampionshipName;
        private string _ChampionshipRound;
        private string _Margin;
        #endregion

        #region Public properties
        /// <summary>
        /// Path to the spreadsheet
        /// </summary>
        public string SpreadSheetPath
        {
            get { return _SpreadSheetPath; }
            set
            {
                if (_SpreadSheetPath == value)
                    return;

                _SpreadSheetPath = value;
                RaisePropertyChanged(nameof(SpreadSheetPath));
                Save();
            }
        }

        /// <summary>
        /// Path to the backing graphic
        /// </summary>
        public string GraphicPath
        {
            get { return _GraphicPath; }
            set
            {
                if (_GraphicPath == value)
                    return;

                _GraphicPath = value;
                RaisePropertyChanged(nameof(GraphicPath));
                Save();
            }
        }

        /// <summary>
        /// Name of the championship
        /// </summary>
        public string ChampionshipName
        {
            get { return _ChampionshipName; }
            set
            {
                if (_ChampionshipName == value)
                    return;

                _ChampionshipName = value;
                RaisePropertyChanged(nameof(ChampionshipName));
                Save();
            }
        }

        /// <summary>
        /// Name of the championship round
        /// </summary>
        public string ChampionshipRound
        {
            get { return _ChampionshipRound; }
            set
            {
                if (_ChampionshipRound == value)
                    return;

                _ChampionshipRound = value;
                RaisePropertyChanged(nameof(ChampionshipRound));
                Save();
            }
        }


        /// <summary>
        /// Vertical margin
        /// </summary>
        public string Margin
        {
            get { return _Margin; }
            set
            {
                if (_Margin == value)
                    return;

                var m = 0;

                if (!int.TryParse(value, out m))
                    value = string.Empty;

                _Margin = value;
                RaisePropertyChanged(nameof(Margin));
                Save();
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Gets the results from the spreadsheet
        /// </summary>
        /// <returns></returns>
        private List<Racer> GetResults()
        {
            var spreadsheet = new ExcelPackage(new FileInfo(SpreadSheetPath));
            var sheet = spreadsheet.Workbook.Worksheets[1];
            var results = new List<Racer>();
            var row = 5;
            var pos = 1;

            //
            //  Keep going until no more names
            //
            while (true)
            {
                if (sheet.Cells[row, 1].Value == null)
                    break;

                //
                // Extract the racer position, name and points
                //
                var racer = new Racer
                {
                    Position = pos,
                    Name = sheet.Cells[row, 1].Value.ToString(),
                    Points = int.Parse(sheet.Cells[row, 2].Value.ToString())
                };

                results.Add(racer);
                pos++;
                row++;
            }

            return results;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Generates the Bitmap
        /// </summary>
        internal void Generate()
        {
            using (var generator = new ResultGenerator())
            {
                generator.BorderY = int.Parse(_Margin);
                generator.GraphicPath = GraphicPath;
                generator.ChampionshipName = ChampionshipName;
                generator.ChampionshipRound = ChampionshipRound;
                generator.Results = GetResults();
                generator.Generate();

                var i = 0;
                foreach(var img in generator.ResultImages)
                {
                    img.Save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), string.Format("Result{0}.bmp", i)));
                    i++;
                }                
            }
        }

        /// <summary>
        /// Saves the settings
        /// </summary>
        internal void Save()
        {
            if (_Loading)
                return;

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(this);
            var filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Constants.DataFile);
            File.WriteAllText(filePath, json);
        }

        internal void Load()
        {
            try
            {
                _Loading = true;

                //
                //  Deserialize the json
                //
                var filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Constants.DataFile);

                if (!File.Exists(filePath))
                    return;

                var json = File.ReadAllText(filePath);
                var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<ViewModelMain>(json);

                //
                // Quick and dirty map of the data onto this instance
                //
                var props = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

                foreach (var prop in props)
                {
                    var data = prop.GetValue(obj);
                    prop.SetValue(this, data);
                }
            }
            finally
            {
                _Loading = false;
            }
        }
        #endregion

    }
}
