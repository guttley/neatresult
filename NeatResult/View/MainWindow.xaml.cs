﻿using MahApps.Metro.Controls;
using NeatResult.ViewModel;
using System;
using System.IO;
using System.Reflection;

namespace NeatResult.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        /// <summary>
        /// View model help for direct accss
        /// </summary>
        internal ViewModelMain ViewModel { get { return DataContext as ViewModelMain; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Spreadsheet button handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSpreadSheetLookup_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            using (var fileDialog = new System.Windows.Forms.OpenFileDialog())
            {
                var result = fileDialog.ShowDialog();
                switch (result)
                {
                    case System.Windows.Forms.DialogResult.OK:
                        ViewModel.SpreadSheetPath = fileDialog.FileName;
                        break;
                    case System.Windows.Forms.DialogResult.Cancel:
                    default:
                        ViewModel.SpreadSheetPath = string.Empty;
                        break;
                }
            }
        }

        /// <summary>
        /// Graphics lookup handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGraphicsLookup_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            using (var fileDialog = new System.Windows.Forms.OpenFileDialog())
            {
                var result = fileDialog.ShowDialog();
                switch (result)
                {
                    case System.Windows.Forms.DialogResult.OK:
                        ViewModel.GraphicPath = fileDialog.FileName;
                        break;
                    case System.Windows.Forms.DialogResult.Cancel:
                    default:
                        ViewModel.GraphicPath = string.Empty;
                        break;
                }
            }
        }

        private void btnGenerate_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                ViewModel.Generate();

                System.Windows.Forms.MessageBox.Show(string.Format("Results generated please see the Result bmp in: {0}", Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)));
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void MetroWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Load();
        }
    }
}
