﻿namespace NeatResult
{
    /// <summary>
    /// Application constants
    /// </summary>
    internal sealed class Constants
    {
        /// <summary>
        /// Filename of the data file to save to
        /// </summary>
        internal const string DataFile = "NeatResult.json";
    }
}